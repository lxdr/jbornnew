package ru.jborn.core.golub.alexander.Lesson9;

import ru.jborn.core.golub.alexander.Lesson9.annotations.DateSince;
import ru.jborn.core.golub.alexander.Lesson9.annotations.Min;
import ru.jborn.core.golub.alexander.Lesson9.annotations.NotEmpty;
import ru.jborn.core.golub.alexander.Lesson9.annotations.Valid;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.time.LocalDate;

public class ClassValidator {

	public boolean isValid(Person person) throws IllegalAccessException {
		Annotation ann = person.getClass().getAnnotation(Valid.class);
		if (ann != null) {
			Field[] fields = person.getClass().getDeclaredFields();
			for (Field field : fields) {
				String type = field.getType().getSimpleName();
				switch (type) {
					case "int":
						if (field.getAnnotation(Min.class) != null) {
							field.setAccessible(true);
							int value = field.getAnnotation(Min.class).value();
							int fieldValue = field.getInt(person);
							if (fieldValue < value) {
								throw new ValidationException("Валидация не пройдена ");
							}
						}
						continue;
					case "String":
						if (field.getAnnotation(NotEmpty.class) != null) {
							String value;
							field.setAccessible(true);
							value = (String) field.get(person);
							if (value.length() < 1) {
								throw new ValidationException("Валидация не пройдена ");
							}
						}
						continue;
					case "LocalDate":
						if (field.getAnnotation(DateSince.class) != null) {
							Annotation annotation = field.getAnnotation(DateSince.class);
							int day = ((DateSince) annotation).day();
							int month = ((DateSince) annotation).month();
							int year = ((DateSince) annotation).year();
							field.setAccessible(true);
							LocalDate dateField = (LocalDate) field.get(person);
							LocalDate dateAnnotation = LocalDate.of(year, month, day);
							if (dateField.isBefore(dateAnnotation)) {
								throw new ValidationException("Валидация не пройдена ");
							}
						}
				}
			}
		}
		return true;
	}
}