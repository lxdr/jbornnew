package ru.jborn.core.golub.alexander.Lesson9;

import ru.jborn.core.golub.alexander.Lesson9.annotations.DateSince;
import ru.jborn.core.golub.alexander.Lesson9.annotations.Min;
import ru.jborn.core.golub.alexander.Lesson9.annotations.NotEmpty;
import ru.jborn.core.golub.alexander.Lesson9.annotations.Valid;

import java.time.LocalDate;

@Valid
public class Person {

    @NotEmpty
    private String name;

    @Min(15)
    private int label;

    @DateSince(
            day = 15,
            month = 12,
            year = 2016
    )
    private LocalDate date;

    Person(String name, int label, LocalDate date) {
        this.name = name;
        this.label = label;
        this.date = date;
    }
}
