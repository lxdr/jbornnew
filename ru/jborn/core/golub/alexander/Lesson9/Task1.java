package ru.jborn.core.golub.alexander.Lesson9;

import java.time.LocalDate;

public class Task1 {

    public static void main(String[] args) {
        Person person = new Person("Max", 44, LocalDate.of(2017, 4, 12));
        ClassValidator validator = new ClassValidator();
        try {
            if (validator.isValid(person)) {
                System.out.println("Валидация пройдена ");
            }else {
                System.out.println("Валидация не пройдена ");
            }
        } catch (IllegalAccessException e) {
            System.out.println("Нет доступа к полю " + e);
        }
    }
}