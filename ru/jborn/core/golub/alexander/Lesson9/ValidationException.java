package ru.jborn.core.golub.alexander.Lesson9;

public class ValidationException extends RuntimeException {

    public ValidationException(String message) {
        super(message);
    }
}
