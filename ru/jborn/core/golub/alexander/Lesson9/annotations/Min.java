package ru.jborn.core.golub.alexander.Lesson9.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Min. Для полей типа int (Integer).
 * Имеет 1 параметр (минимальное допустимое значение).
 * Если значение поля меньше, чем указано в аннотации, то ошибка валидации
 * */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Min {
    int value();
}
