package ru.jborn.core.golub.alexander.lesson2;

public class Circle implements ShapeMethods {
    double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double area() {
        return Math.PI * radius * radius;
    }
}
