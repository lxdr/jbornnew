package ru.jborn.core.golub.alexander.lesson2;

public interface ShapeMethods {
    double area();

    default int compare(double j) {
        return Double.compare(this.area(), j);
    }
}
