package ru.jborn.core.golub.alexander.lesson2;

public class Square implements ShapeMethods {
    double side;

    public Square(double side) {
        this.side = side;
    }

    @Override
    public double area() {
        return side * side;
    }
}
