package ru.jborn.core.golub.alexander.lesson2;

public class Task1 {
    private int to_count(int h) {
        return h / 10 + h % 10;
    }

    public static void main(String[] args) {
        Task1 t = new Task1();
        int h = 66;

        System.out.println(t.to_count(h));
    }
}