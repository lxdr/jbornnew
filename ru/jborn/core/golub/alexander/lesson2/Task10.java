package ru.jborn.core.golub.alexander.lesson2;

public class Task10 {
    private String nameOfDay(int day) {
        switch (day) {
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
            case 7:
                return "Sunday";
            default:
                return "enter the other number of day";
        }
    }

    public static void main(String[] args) {
        Task10 t = new Task10();
        int day = 5;
        System.out.println(t.nameOfDay(day));
    }
}
