package ru.jborn.core.golub.alexander.lesson2;

public class Task11 {
    private String[] calculate(int x) {
        String[] result = new String[10];
        for (int i = 0; i < 10; i++) {
            result[i] = String.valueOf(x * i);
        }
        return result;
    }

    public static void main(String[] args) {
        int x = 5;
        Task11 t = new Task11();
        int i = 0;
        for (String s : t.calculate(x)) {
            System.out.println(s);
            for (int j = 0; j <= i; j++) {
                System.out.print(" ");
            }
            i++;
        }
    }
}
