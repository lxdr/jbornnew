package ru.jborn.core.golub.alexander.lesson2;

public class Task14 {
    private static void triangle(int x) {
        for (int i = 0; i <= x; i++) {
            for (int j = 0; j < i; j++)
                System.out.print("*");
            System.out.println();
        }
    }

    public static void main(String[] args) {
        int x = 5;
        triangle(5);
    }
}