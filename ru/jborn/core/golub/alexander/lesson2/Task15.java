package ru.jborn.core.golub.alexander.lesson2;

public class Task15 {
    private static void triangle(int x) {
        if ((x % 2) != 0) {
            x = x + 1;
        }
        int n = 0;
        for (int i = x / 2; i >= 0; i--) {
            n++;
            for (int j = 0; j <= i; j++) {
                System.out.print(" ");
            }
            for (int m = 0; m < n; m++) {
                System.out.print("*");
            }
            n++;
            System.out.println();
        }
    }

    public static void main(String[] args) {
        triangle(25);
    }
}