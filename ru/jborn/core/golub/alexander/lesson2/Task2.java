package ru.jborn.core.golub.alexander.lesson2;

public class Task2 {
    private double toCount(int x) {
        double y;
        if (x > 0) y = Math.sin(x) * Math.sin(x);
        else y = 1 - (2 * Math.sin(x * x));
        return y;
    }

    public static void main(String[] args) {
        int x = -2;
        Task2 t = new Task2();
        System.out.println(t.toCount(x));
    }
}

