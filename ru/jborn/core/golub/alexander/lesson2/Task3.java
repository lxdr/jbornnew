package ru.jborn.core.golub.alexander.lesson2;

public class Task3 {
    private int max(int i, int j) {
        int k;
        if (i > j) k = i;
        else k = j;
        return k;
    }

    private int min(int i, int j) {
        int k;
        if (i > j) k = j;
        else k = i;
        return k;
    }

    public static void main(String[] args) {
        Task3 t = new Task3();
        int i = 3;
        int j = 5;

        System.out.println(t.max(i, j));
        System.out.println(t.min(i, j));
    }

}
