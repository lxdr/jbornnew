package ru.jborn.core.golub.alexander.lesson2;

public class Task4 {
    private static int compare(double distanceInKm, double distanceInFoot) {
        double m = distanceInKm * 1000;
        double p = distanceInFoot * 1000 * 0.305;

        if (m > p) {
            return 1;
        } else {
            if (p > m) return -1;
        }
        return 0;
    }

    public static void main(String[] args) {
        int distanceInKm = 2;
        int distanceInFoot = 6;
        if (compare(distanceInKm, distanceInFoot) == 1) {
            System.out.println("metre > foot");
        } else if (compare(distanceInKm, distanceInFoot) == -1) {
            System.out.println("foot > metre");
        } else {
            System.out.println("it's the same");
        }
    }
}