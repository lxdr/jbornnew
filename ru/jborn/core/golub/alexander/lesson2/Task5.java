package ru.jborn.core.golub.alexander.lesson2;

public class Task5 {
    public static void main(String[] args) {
        Circle circle = new Circle(5);
        Square square = new Square(4);

        int i = circle.compare(square.area());

        if ( i == 1) System.out.println("Circle >");
        else if (i == -1) System.out.println("Circle <");
        else System.out.println("The same");
    }
}
