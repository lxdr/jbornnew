package ru.jborn.core.golub.alexander.lesson2;

public class Task7 {
    private static int inverse(int value) {
        int result = 0;
        while (value > 0) {
            result = result * 10 + value % 10;
            value /= 10;
        }
        return result;
    }

    private static void compare(int digit) {
        int digit2 = inverse(digit);
        if (digit == digit2) {
            System.out.println("the numbers are equal to each other");
        } else
            System.out.println("the numbers are not equal to each other");
    }

    public static void main(String[] args) {
        compare(999);
    }
}
