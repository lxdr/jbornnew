package ru.jborn.core.golub.alexander.lesson2;

public class Task8 {
    private String sameNumbers(Integer value) {
        String val = value.toString();
        for (int i = 0; i < val.length(); i++) {
            for (int j = i+1; j < val.length(); j++) {
                if (val.charAt(i) == val.charAt(j)) {
                    return "********Есть одинаковые числа******";
                }
            }
        }
        return "Нет одинаковых чисел";
    }

    public static void main(String[] args) {
        Task8 t = new Task8();
        System.out.println(t.sameNumbers(801));
    }
}