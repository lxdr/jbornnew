package ru.jborn.core.golub.alexander.lesson2;

public class Task9 {
    private double f(double x) {
        if (x <= 0) {
            return 0;
        } else if (x <= 1) {
            return x;
        }
        return x * x;
    }

    public static void main(String[] args) {
        Task9 t = new Task9();
        double x = 2.5;
        System.out.println(t.f(x));
    }
}
