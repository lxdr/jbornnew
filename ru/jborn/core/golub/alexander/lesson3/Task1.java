package ru.jborn.core.golub.alexander.lesson3;

public class Task1 {
    private String reverse(String s) {
        StringBuilder builder = new StringBuilder();
        char[] wToCh = s.toCharArray();
        for (int i = wToCh.length - 1; i >= 0; i--) {
            builder = builder.append(wToCh[i]);
        }
        return builder.toString();
    }

    public static void main(String[] args) {
        Task1 t = new Task1();
        String s = "hello";
        System.out.println(t.reverse(s));
    }
}