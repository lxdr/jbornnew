package ru.jborn.core.golub.alexander.lesson3;

public class Task2 {

    private String stars(String s) {
        StringBuilder getStars = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            getStars.append('*');
        }
        return getStars.toString();
    }

    public static void main(String[] args) {
        String word = "hello";

        Task2 t = new Task2();
        String stars = t.stars(word);
        System.out.println(stars + word + stars);
    }
}