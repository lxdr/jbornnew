package ru.jborn.core.golub.alexander.lesson3;

public class Task3 {
    private int count(String string, char j) {
        int count = 0;
        for (int i = 0; i < string.length() - 1; i++) {
            if (string.charAt(i) == j) count++;
        }
        return count;
    }


    public static void main(String[] args) {
        String s = "Дан текст и символ. Определить количество данного символа в тексте.";
        Task3 t = new Task3();
        System.out.println(t.count(s, 'и'));
    }
}