package ru.jborn.core.golub.alexander.lesson3;

public class Task4 {
    private String text;

    private int countWord(String s) {
        return s.split(" ").length;
    }

    public static void main(String[] args) {
        Task4 t = new Task4();
        t.text = "Дан текст. В нем слова разделены одним пробелом (начальные и конечные пробелы и символ " +
                "\"-\" в тексте отсутствуют). Определить количество слов в тексте.";
        System.out.println(t.countWord(t.text));
    }
}
