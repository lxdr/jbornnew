package ru.jborn.core.golub.alexander.lesson3;


public class Task5 {

    private boolean compare(String word) {
        System.out.println(word.length());
        for (int i = 0; i < word.length() / 2; i++) {
            if (word.charAt(i) != word.charAt((word.length() - 1) - i)) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {

        Task5 t = new Task5();
        String s = "HelloJborn";
        System.out.println(t.compare(s));

    }

}
