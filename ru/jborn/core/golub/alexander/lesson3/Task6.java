package ru.jborn.core.golub.alexander.lesson3;

public class Task6 {
    private String s = "324578";

    private int getSum(String s) {
        int y = Integer.parseInt(s);
        int result = 0;

        while (y > 0) {
            result = result + (y % 10);
            y /= 10;
        }
        return result;
    }

    public static void main(String[] args) {
        Task6 t = new Task6();
        System.out.println(t.getSum(t.s));
    }
}
