package ru.jborn.core.golub.alexander.lesson4;

public class Circle extends ShapeWithArea {
    double radius;
    Point centre;

    public Circle( double radius, int x, int y ) {
        this.radius = radius;
        this.centre = centre;
    }

    @Override
    double area() {
        return radius * radius * Math.PI;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", centre=" + centre +
                '}';
    }
}
