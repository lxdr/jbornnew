package ru.jborn.core.golub.alexander.lesson4;

public class Oop {
    public static void main( String[] args ) {
        Circle yelloy = new Circle(12, 0, 0);
        Circle red = new Circle(11, 3, 5);
        String val = "меньше";

        if (yelloy.compareByArea(red) > 0) {
            val = "больше";
        }
        if (yelloy.compareByArea(red) == 0){
            val = "равны";
        }

        System.out.println(yelloy.toString() + " " + "\n" + val + "\n" + red.toString());
    }
}
