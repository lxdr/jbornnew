package ru.jborn.core.golub.alexander.lesson4;

public abstract class ShapeWithArea {
    abstract double area();

    /**
     * Метод, который сравнивает объект у которого вызывается этот метод, с объектом, который передается в качестве
     * аргумента.
     *
     * @param shape
     * @return Возвращает 1, если значение площади объекта вызывающего метод больше значения передающегося в качестве параметра.
     * 0 - если площади равны.
     * -1 - если значение площади объекта передающегося в качестве параметра больше значения вызывающего метод.
     */
    int compareByArea( ShapeWithArea shape ) {
        if (this.area() > shape.area()) {
            return 1;
        }
        if (this.area() > shape.area()) {
            return 0;
        }
        return -1;
    }
}
