package ru.jborn.core.golub.alexander.lesson5.task1;

import ru.jborn.core.golub.alexander.lesson5.task1.GovNumber;

import java.time.LocalDate;

public class Car implements Cloneable{

    private String brand;
    private String model;
    private GovNumber govNumber;
    private LocalDate prodDate;
    private double mileage;

    public Car( String brand, String model, GovNumber govNumber, LocalDate prodDate, double mileage ) {
        this.brand = brand;
        this.model = model;
        this.govNumber = govNumber;
        this.prodDate = prodDate;
        this.mileage = mileage;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand( String brand ) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel( String model ) {
        this.model = model;
    }

    public GovNumber getGovNumber() {
        return govNumber;
    }

    public void setGovNumber( GovNumber govNumber ) {
        this.govNumber = govNumber;
    }

    public LocalDate getProdDate() {
        return prodDate;
    }

    public Double getMileage() {
        return mileage;
    }

    public void setProdDate( LocalDate prodDate ) {
        this.prodDate = prodDate;
    }


    public void setMileage( double mileage ) {
        this.mileage = mileage;
    }

    public Car deepClone() throws CloneNotSupportedException {
        Car clone = (Car) super.clone();
        clone.govNumber = govNumber.clone();
        clone.prodDate = LocalDate.of(prodDate.getYear(), prodDate.getMonth(), prodDate.getDayOfMonth());
        return clone;
    }

    @Override
    public Car clone() throws CloneNotSupportedException {
        return (Car) super.clone();
    }

    @Override
    public String toString() {
        return " Car" + " {" + brand + " " + model + " " + govNumber.toString() + " " + prodDate.toString() + " " + mileage + "}";
    }
}