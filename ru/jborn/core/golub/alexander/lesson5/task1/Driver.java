package ru.jborn.core.golub.alexander.lesson5.task1;


import ru.jborn.core.golub.alexander.lesson5.task1.Car;

import java.time.LocalDate;

public class Driver implements Cloneable{


    private String name;
    private String surname;
    private String middleName;
    private LocalDate birthDate;
    private int experience;
    private Car car;

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname( String surname ) {
        this.surname = surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName( String middleName ) {
        this.middleName = middleName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate( LocalDate birthDate ) {
        this.birthDate = birthDate;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience( int experience ) {
        this.experience = experience;
    }

    public Car getCar() {
        return car;
    }

    public Driver( String name, String surname, String middleName, LocalDate birthDate, int experience, Car car ) {
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
        this.birthDate = birthDate;
        this.experience = experience;
        this.car = car;
    }

    public void setCar( Car car ) {
        this.car = car;
    }

    @Override
    public Driver clone() throws CloneNotSupportedException {
        return (Driver) super.clone();
    }

    public Driver deepClone() throws CloneNotSupportedException {
        Driver clone = (Driver) super.clone();
        clone.birthDate = LocalDate.of(birthDate.getYear(), birthDate.getMonth(), birthDate.getDayOfMonth());
        clone.car = car.deepClone();
        return clone;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", middleName='" + middleName + '\'' +
                ", birthDate=" + birthDate +
                ", experience=" + experience + '}' + car.toString();
    }
}
