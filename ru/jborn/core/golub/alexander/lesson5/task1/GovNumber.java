package ru.jborn.core.golub.alexander.lesson5.task1;

public class GovNumber implements Cloneable {

    private String firstLetters;
    private int digit;
    private String lastLetters;
    private int region;

    public GovNumber( String firstLetters, Integer digit, String lastLetters, Integer region ) {
    }
    public GovNumber( ) {
    }

    public GovNumber(String firstLetters, int digit, String lastLetters, int region) {
        this.firstLetters = firstLetters;
        this.digit = digit;
        this.lastLetters = lastLetters;
        this.region = region;
    }

    @Override
    public String toString() {
        return '\'' + firstLetters + digit + lastLetters + '\'' + region;
    }

    @Override
    public GovNumber clone() throws CloneNotSupportedException {
        return (GovNumber) super.clone();
    }
}
