package ru.jborn.core.golub.alexander.lesson5.task1;


import java.time.LocalDate;

public class Task1 implements Cloneable {

    public static void main( String[] args ) {
        LocalDate prodDate = LocalDate.of(2009, 12, 12);
        GovNumber govNumber = new GovNumber("вв", 345, "в", 55);
        LocalDate bDay = LocalDate.of(1989, 7, 31);

        Car yellow = new Car("lada", "samara", govNumber, prodDate, 55000);
        Driver driver = new Driver("Vasya", "Golovin", "Valentinovich", bDay, 20, yellow);

        GovNumber govNumber1 = new GovNumber("ак", 777, "а", 77);
        LocalDate prodDate1 = LocalDate.of(2013, 2, 3);
        Car red = new Car("Subaru", "forester", govNumber1, prodDate1, 10000);

        try {
            Car cloneRed = red.deepClone();
            Driver driver1 = driver.deepClone();
            System.out.println(driver1.toString());
            driver1.setName("I ");
            driver1.setSurname("am ");
            driver1.setMiddleName("CLONE");
            driver1.setBirthDate(LocalDate.of(1990, 8, 12));
            driver1.setCar(cloneRed);
            System.out.println(driver1.toString());
        } catch (CloneNotSupportedException e) {
            System.out.print("Клонирование невозможно");
        }
        System.out.println(driver.toString());
    }
}
