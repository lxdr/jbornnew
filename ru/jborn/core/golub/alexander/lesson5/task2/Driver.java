package ru.jborn.core.golub.alexander.lesson5.task2;

import java.time.LocalDate;

public class Driver implements Cloneable{

    private String name;
    private String surname;
    private String middleName;
    private LocalDate birthDate;
    private int experience;
    private Car car;

    public Driver( String name, String surname, String middleName, LocalDate birthDate, int experience, Car car ) {
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
        this.birthDate = birthDate;
        this.experience = experience;
        this.car = car;
    }

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname( String surname ) {
        this.surname = surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName( String middleName ) {
        this.middleName = middleName;
    }

    public void setBirthDate( LocalDate birthDate ) {
        this.birthDate = birthDate;
    }

    public void setExperience( int experience ) {
        this.experience = experience;
    }

    public Car getCar() {
        return car;
    }

    public void setCar( Car car ) {
        this.car = car;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public int getExperience() {
        return experience;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", middleName='" + middleName + '\'' +
                ", birthDate=" + birthDate +
                ", experience=" + experience + '}' + car.toString();
    }


}


