package ru.jborn.core.golub.alexander.lesson5.task2;

public class GovNumber {

    private String firstLetters;
    private int digit;
    private String lastLetters;
    private int region;

    public GovNumber( String firstLetters, Integer digit, String lastLetters, Integer region ) {
    }
    public GovNumber( ) {
    }

    public GovNumber(String firstLetters, int digit, String lastLetters, int region) {
        this.firstLetters = firstLetters;
        this.digit = digit;
        this.lastLetters = lastLetters;
        this.region = region;
    }

    public String getFirstLetters() {
        return firstLetters;
    }

    public void setFirstLetters( String firstLetters ) {
        this.firstLetters = firstLetters;
    }

    public int getDigit() {
        return digit;
    }

    public void setDigit( int digit ) {
        this.digit = digit;
    }

    public String getLastLetters() {
        return lastLetters;
    }

    public void setLastLetters( String lastLetters ) {
        this.lastLetters = lastLetters;
    }

    public int getRegion() {
        return region;
    }

    public void setRegion( int region ) {
        this.region = region;
    }

    @Override
    public String toString() {
        return '\'' + firstLetters + digit + lastLetters + '\'' + region;
    }
}
