package ru.jborn.core.golub.alexander.lesson5.task2;


import ru.jborn.core.golub.alexander.lesson5.task2.validators.CarValidator;
import ru.jborn.core.golub.alexander.lesson5.task2.validators.DriverValidator;
import ru.jborn.core.golub.alexander.lesson5.task2.validators.GovNumberValidator;
import ru.jborn.core.golub.alexander.lesson5.task2.validators.Validator;

import java.time.LocalDate;

public class task2 {
    public static void main( String[] args ) {
        Validator<Driver> driverValidator = new DriverValidator();
        Validator<Car> carValidator = new CarValidator();
        Validator<GovNumber> govNumberValidator = new GovNumberValidator();
        LocalDate birthDate = LocalDate.of(1989, 5, 6);
        GovNumber govNumber = new GovNumber("ук", 333, "о", 55);
        LocalDate day = LocalDate.of(1989, 8, 18);
        Car red;
        red = new Car("Mersedes", "slk5", govNumber, day, 123455);
        Driver dave = new Driver("Dave", "Sanders", "Jonovich", birthDate, 39, red);

        if (govNumberValidator.isValid(govNumber) && carValidator.isValid(red) && driverValidator.isValid(dave)) {
             System.out.println("**********************************************************");
        }
    }
}
