package ru.jborn.core.golub.alexander.lesson5.task2.validators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class AgregateValidator<T> implements Validator<T> {
    private List<Validator<T>> allValidators;

    protected AgregateValidator( Validator<T>... validatorsArg ) {
        allValidators = new ArrayList(Arrays.asList(validatorsArg));
    }

    @Override
    public boolean isValid( T ob ) {
        boolean correct;
        for (Validator<T> validator : allValidators) {
            correct = validator.isValid(ob);
            if (!correct) {
                return false;
            }
        }
        return true;
    }
}