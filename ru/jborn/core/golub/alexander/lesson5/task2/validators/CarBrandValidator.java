package ru.jborn.core.golub.alexander.lesson5.task2.validators;

import ru.jborn.core.golub.alexander.lesson5.task2.Car;

public class CarBrandValidator implements Validator<Car> {
    @Override
    public boolean isValid( Car ob ) {
        return ob != null || ob.getBrand().isEmpty() || ob.getBrand().matches(Constants.STRING_REG);
    }
}

