package ru.jborn.core.golub.alexander.lesson5.task2.validators;


import ru.jborn.core.golub.alexander.lesson5.task2.Car;

public class CarMileageValidator implements Validator<Car> {
    /* проверяется, что есть число и это число больше минимального пробега нового авто*/
    @Override
    public boolean isValid( Car ob ) {
        return ob.getMileage() != 0 && ob.getMileage() > 700;
    }
}
