package ru.jborn.core.golub.alexander.lesson5.task2.validators;

import ru.jborn.core.golub.alexander.lesson5.task2.Car;

public class CarModelValidator implements Validator<Car> {
    @Override
    public boolean isValid( Car ob ) {
        return ob.getModel() != null || ob.getModel().isEmpty() || ob.getModel().matches(Constants.STRING_REG);
    }
}
