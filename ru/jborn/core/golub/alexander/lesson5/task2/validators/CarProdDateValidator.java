package ru.jborn.core.golub.alexander.lesson5.task2.validators;

import ru.jborn.core.golub.alexander.lesson5.task2.Car;

import java.time.LocalDate;

public class CarProdDateValidator implements Validator<Car> {
    @Override
    public boolean isValid( Car ob ) {
        return !ob.getProdDate().isBefore(Constants.DATE_BIRTHDAY_PRODUCTION);
    }
}
