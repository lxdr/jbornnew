package ru.jborn.core.golub.alexander.lesson5.task2.validators;

import ru.jborn.core.golub.alexander.lesson5.task2.Car;

public class CarValidator extends AgregateValidator<Car> {
    public CarValidator() {
        super(new CarBrandValidator(), new CarMileageValidator(), new CarModelValidator(), new CarProdDateValidator());
    }
}
