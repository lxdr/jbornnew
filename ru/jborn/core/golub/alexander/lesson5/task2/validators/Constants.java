package ru.jborn.core.golub.alexander.lesson5.task2.validators;

import java.time.LocalDate;

public final class Constants {
    public static final String STRING_REG = "[-+]?\\d+";
    public static final LocalDate DATE_BIRTHDAY_PRODUCTION = LocalDate.of(1970, 1, 1);
}
