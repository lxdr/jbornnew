package ru.jborn.core.golub.alexander.lesson5.task2.validators;


import ru.jborn.core.golub.alexander.lesson5.task2.Driver;

import java.time.LocalDate;

public class DriverBirthDateValidator implements Validator<Driver> {
    @Override
    public boolean isValid( Driver ob ) {
        return !ob.getBirthDate().isBefore(Constants.DATE_BIRTHDAY_PRODUCTION);
    }
}