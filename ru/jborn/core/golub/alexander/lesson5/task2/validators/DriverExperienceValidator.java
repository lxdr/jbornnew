package ru.jborn.core.golub.alexander.lesson5.task2.validators;


import ru.jborn.core.golub.alexander.lesson5.task2.Driver;

public class DriverExperienceValidator implements Validator<Driver> {
    @Override
    public boolean isValid( Driver ob ) {
        return ob.getExperience() != 0;
    }
}
