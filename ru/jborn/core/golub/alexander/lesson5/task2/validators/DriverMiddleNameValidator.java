package ru.jborn.core.golub.alexander.lesson5.task2.validators;


import ru.jborn.core.golub.alexander.lesson5.task2.Driver;

public class DriverMiddleNameValidator implements Validator<Driver> {
    @Override
    public boolean isValid( Driver ob ) {
        return ob.getMiddleName() != null || ob.getMiddleName().isEmpty() || ob.getMiddleName().matches(Constants.STRING_REG);
    }
}
