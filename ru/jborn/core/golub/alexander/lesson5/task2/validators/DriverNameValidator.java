package ru.jborn.core.golub.alexander.lesson5.task2.validators;

import ru.jborn.core.golub.alexander.lesson5.task2.Driver;

public class DriverNameValidator implements Validator<Driver> {
    @Override
    public boolean isValid( Driver ob ) {
        return ob.getName() != null || ob.getName().isEmpty() || ob.getName().matches(Constants.STRING_REG);
    }
}
