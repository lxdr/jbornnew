package ru.jborn.core.golub.alexander.lesson5.task2.validators;

import ru.jborn.core.golub.alexander.lesson5.task2.Driver;

public class DriverSurnameValidator implements Validator<Driver> {
    @Override
    public boolean isValid( Driver ob ) {
        return ob != null || ob.getSurname().isEmpty() || ob.getSurname().matches(Constants.STRING_REG);
    }
}
