package ru.jborn.core.golub.alexander.lesson5.task2.validators;

import ru.jborn.core.golub.alexander.lesson5.task2.Driver;

public class DriverValidator extends AgregateValidator<Driver> {
    public DriverValidator() {
        super(new DriverBirthDateValidator(), new DriverExperienceValidator(), new DriverMiddleNameValidator(), new DriverNameValidator(),
                new DriverSurnameValidator());
    }
}
