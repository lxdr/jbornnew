package ru.jborn.core.golub.alexander.lesson5.task2.validators;

import ru.jborn.core.golub.alexander.lesson5.task2.GovNumber;

public class GovNumber1stLetterValidator implements Validator<GovNumber> {
    @Override
    public boolean isValid( GovNumber ob ) {
        return ob.getFirstLetters() != null || ob.getFirstLetters().isEmpty() || ob.getFirstLetters().matches(Constants.STRING_REG);
    }
}
