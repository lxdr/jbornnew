package ru.jborn.core.golub.alexander.lesson5.task2.validators;

import ru.jborn.core.golub.alexander.lesson5.task2.GovNumber;

public class GovNumberEndLettersValidator implements Validator<GovNumber> {
    @Override
    public boolean isValid( GovNumber ob ) {
        return ob.getLastLetters() != null || ob.getLastLetters().isEmpty() || ob.getLastLetters().matches(Constants.STRING_REG);
    }
}

