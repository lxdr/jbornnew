package ru.jborn.core.golub.alexander.lesson5.task2.validators;

import ru.jborn.core.golub.alexander.lesson5.task2.GovNumber;

public class GovNumberNOValidation implements Validator<GovNumber> {
    @Override
    public boolean isValid( GovNumber ob ) {
        return ob.getDigit() != 0;
    }
}
