package ru.jborn.core.golub.alexander.lesson5.task2.validators;

import ru.jborn.core.golub.alexander.lesson5.task2.GovNumber;

public class GovNumberRegionValidator implements Validator<GovNumber> {
    @Override
    public boolean isValid( GovNumber ob ) {
        return ob.getRegion() != 0;
    }
}
