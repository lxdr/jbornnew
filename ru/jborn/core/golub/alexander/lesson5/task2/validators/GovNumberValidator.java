package ru.jborn.core.golub.alexander.lesson5.task2.validators;


import ru.jborn.core.golub.alexander.lesson5.task2.GovNumber;

public class GovNumberValidator extends AgregateValidator<GovNumber> {
    public GovNumberValidator() {
        super(new GovNumber1stLetterValidator(), new GovNumberEndLettersValidator(), new GovNumberNOValidation(),
                new GovNumberRegionValidator());
    }
}
