package ru.jborn.core.golub.alexander.lesson5.task2.validators;

public interface Validator<T> {

    boolean isValid( T ob );
}
