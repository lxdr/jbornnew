package ru.jborn.core.golub.alexander.lesson5.task3;

import java.time.LocalDate;

public class Car {
    private String brand;
    private String model;
    private GovNumber govNumber;
    private LocalDate prodDate;
    private double mileage;

    public Car() {
        this.brand = " none ";
        this.model = " none ";
        this.govNumber = new GovNumber();
        this.prodDate = LocalDate.of(1970, 1, 1);
        this.mileage = 0;
    }

    public Car( String brand, String model, GovNumber govNumber,
                LocalDate prodDate, double mileage ) {
        this.brand = brand;
        this.model = model;
        this.govNumber = govNumber;
        this.prodDate = prodDate;
        this.mileage = mileage;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public LocalDate getProdDate() {
        return prodDate;
    }

    public Double getMileage() {
        return mileage;
    }

    public void setBrand( String brand ) {
        this.brand = brand;
    }

    public void setModel( String model ) {
        this.model = model;
    }

    public void setGovNumber( GovNumber govNumber ) {
        this.govNumber = govNumber;
    }

    public void setProdDate( LocalDate prodDate ) {
        this.prodDate = prodDate;
    }

    public void setMileage( double mileage ) {
        this.mileage = mileage;
    }

    @Override
    public String toString() {
        return " Car" + " {" + brand + " " +
                model + " " +
                govNumber.toString() + " " +
                prodDate.toString() + " " + mileage + "}";
    }
}