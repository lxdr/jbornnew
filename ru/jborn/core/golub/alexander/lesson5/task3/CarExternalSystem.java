package ru.jborn.core.golub.alexander.lesson5.task3;


public class CarExternalSystem {
    private String brandModel;
    private int prodDay;
    private int prodMounth;
    private int prodYear;
    private String govNumber;
    private int region;
    private int milleage;

    public CarExternalSystem( String brandModel, int prod_day,
                              int prod_mounth, int prod_year,
                              String govNumber, int region,
                              int milleage ) {
        this.brandModel = brandModel;
        this.prodDay = prod_day;
        this.prodMounth = prod_mounth;
        this.prodYear = prod_year;
        this.govNumber = govNumber;
        this.region = region;
        this.milleage = milleage;
    }

    public String getBrandModel() {
        return brandModel;
    }

    public void setBrandModel( String brandModel ) {
        this.brandModel = brandModel;
    }

    public int getProdDay() {
        return prodDay;
    }

    public void setProdDay( int prodDay ) {
        this.prodDay = prodDay;
    }

    public int getProdMounth() {
        return prodMounth;
    }

    public void setProdMounth( int prodMounth ) {
        this.prodMounth = prodMounth;
    }

    public int getProdYear() {
        return prodYear;
    }

    public void setProdYear( int prodYear ) {
        this.prodYear = prodYear;
    }

    public String getGovNumber() {
        return govNumber;
    }

    public void setGovNumber( String govNumber ) {
        this.govNumber = govNumber;
    }

    public int getRegion() {
        return region;
    }

    public void setRegion( int region ) {
        this.region = region;
    }

    public int getMilleage() {
        return milleage;
    }

    public void setMilleage( int milleage ) {
        this.milleage = milleage;
    }
}
