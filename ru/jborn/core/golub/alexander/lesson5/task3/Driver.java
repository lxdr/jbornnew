package ru.jborn.core.golub.alexander.lesson5.task3;

import java.time.LocalDate;

public class Driver {

    private String name;
    private String surname;
    private String middleName;
    private LocalDate birthDate;
    private int experience;
    private Car car;

    public Driver( String name, String surname, String middleName, LocalDate birthDate, int experience, Car car ) {
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
        this.birthDate = birthDate;
        this.experience = experience;
        this.car = car;
    }

    public Driver() {
        this.name = null;
        this.surname = null;
        this.middleName = null;
        this.birthDate = LocalDate.of(1917, 10, 25);
        this.experience = 0;
        this.car = null;
    }

    ;

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public int getExperience() {
        return experience;
    }

    public Car getCar() {
        return car;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public void setSurname( String surname ) {
        this.surname = surname;
    }

    public void setMiddleName( String middleName ) {
        this.middleName = middleName;
    }

    public void setBirthDate( LocalDate birthDate ) {
        this.birthDate = birthDate;
    }

    public void setExperience( int experience ) {
        this.experience = experience;
    }

    public void setCar( Car car ) {
        this.car = car;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", middleName='" + middleName + '\'' +
                ", birthDate=" + birthDate +
                ", experience=" + experience + '}' + car.toString();
    }
}
