package ru.jborn.core.golub.alexander.lesson5.task3;

public class DriverExternalSystem {
    private String nameFull;
    private int birthDay;
    private int birthMounth;
    private int birthYear;
    private int experience;

    public DriverExternalSystem( String nameFull, int birthDay, int birthMounth, int birthYear, int experience ) {
        this.nameFull = nameFull;
        this.birthDay = birthDay;
        this.birthMounth = birthMounth;
        this.birthYear = birthYear;
        this.experience = experience;
    }

    public String getNameFull() {
        return nameFull;
    }

    public void setNameFull( String nameFull ) {
        this.nameFull = nameFull;
    }

    public int getBirthDay() {
        return birthDay;
    }

    public void setBirthDay( int birthDay ) {
        this.birthDay = birthDay;
    }

    public int getBirthMounth() {
        return birthMounth;
    }

    public void setBirthMounth( int birthMounth ) {
        this.birthMounth = birthMounth;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear( int birthYear ) {
        this.birthYear = birthYear;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience( int experience ) {
        this.experience = experience;
    }
}
