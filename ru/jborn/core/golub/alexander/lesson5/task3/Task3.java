package ru.jborn.core.golub.alexander.lesson5.task3;

import ru.jborn.core.golub.alexander.lesson5.task3.convertor.CarConvertor;
import ru.jborn.core.golub.alexander.lesson5.task3.convertor.Convertor;
import ru.jborn.core.golub.alexander.lesson5.task3.convertor.DriverConvertor;

public class Task3 {
    public static void main( String[] args ) {
        CarExternalSystem carExternalSystem = new CarExternalSystem("volvo xc90", 1, 12, 2016, "см212м",
                77, 55000);
        DriverExternalSystem driverExternalSystem = new DriverExternalSystem("Niklaus Emil Wirth", 15, 02, 1934,
                20);

        Convertor<Car, CarExternalSystem> carConvertor = new CarConvertor();
        Car red = carConvertor.convert(carExternalSystem);
        System.out.println(red);
        Convertor<Driver, DriverExternalSystem> driverConvertor = new DriverConvertor();
        Driver nikola = driverConvertor.convert(driverExternalSystem);
        if (nikola.getCar() == null) {
            nikola.setCar(red);
        }
        System.out.println(nikola);
    }
}
