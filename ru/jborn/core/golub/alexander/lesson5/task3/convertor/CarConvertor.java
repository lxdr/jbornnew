package ru.jborn.core.golub.alexander.lesson5.task3.convertor;


import ru.jborn.core.golub.alexander.lesson5.task3.Car;
import ru.jborn.core.golub.alexander.lesson5.task3.CarExternalSystem;
import ru.jborn.core.golub.alexander.lesson5.task3.GovNumber;

import java.time.LocalDate;

public class CarConvertor implements Convertor<Car, CarExternalSystem> {

    private LocalDate convertDate(CarExternalSystem carExternalSystem){
        return LocalDate.of(carExternalSystem.getProdYear(), carExternalSystem.getProdMounth(),
                carExternalSystem.getProdDay());
    }

    private GovNumber convertGovNumber(CarExternalSystem carExternalSystem){
        GovNumber govNumberExternalCar;
        char[] firstLettersArray = {carExternalSystem.getGovNumber().charAt(0), carExternalSystem.getGovNumber().charAt(1)};
        String firstLettrs = new String(firstLettersArray);
        String govNumberDigit = carExternalSystem.getGovNumber().substring(2, 3);
        String lastLetter = String.valueOf(carExternalSystem.getGovNumber().charAt(4));

        govNumberExternalCar = new GovNumber(firstLettrs, Integer.valueOf(govNumberDigit), lastLetter,
                carExternalSystem.getRegion());

        return govNumberExternalCar;
    }

    @Override
    public Car convert( CarExternalSystem carExternalSystem ) {
        Car car = new Car();

        String[] brandModelToString = carExternalSystem.getBrandModel().split(" ");
        car.setBrand(brandModelToString[0]);
        car.setModel(brandModelToString[1]);

        car.setMileage(carExternalSystem.getMilleage());
        car.setProdDate(convertDate(carExternalSystem));
        car.setGovNumber(convertGovNumber(carExternalSystem));

        return car;
    }
}