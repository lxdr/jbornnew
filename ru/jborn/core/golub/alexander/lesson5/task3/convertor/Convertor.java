package ru.jborn.core.golub.alexander.lesson5.task3.convertor;

public interface Convertor<T, V> {
    T convert( V v );
}