package ru.jborn.core.golub.alexander.lesson5.task3.convertor;

import ru.jborn.core.golub.alexander.lesson5.task3.Driver;
import ru.jborn.core.golub.alexander.lesson5.task3.DriverExternalSystem;

import java.time.LocalDate;

public class DriverConvertor implements Convertor<Driver, DriverExternalSystem> {

    private LocalDate convertDate(DriverExternalSystem driverExternalSystem){
        return LocalDate.of(driverExternalSystem.getBirthYear(), driverExternalSystem.getBirthMounth(),
                driverExternalSystem.getBirthDay());
    }

    @Override
    public Driver convert( DriverExternalSystem driverExternalSystem ) {
        Driver driver = new Driver();
        driver.setExperience(driverExternalSystem.getExperience());
        driver.setBirthDate(convertDate(driverExternalSystem));

        String[] fullNameArray = driverExternalSystem.getNameFull().split(" ");
        driver.setName(fullNameArray[0]);
        driver.setMiddleName(fullNameArray[1]);
        driver.setSurname(fullNameArray[2]);

        return driver;
    }
}