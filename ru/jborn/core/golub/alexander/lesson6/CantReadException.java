package ru.jborn.core.golub.alexander.lesson6;

public class CantReadException extends Exception {
    public CantReadException( String message ) {
        super(message);
    }
}


