package ru.jborn.core.golub.alexander.lesson6;

public class CantWriteInFile extends Exception {
    public CantWriteInFile( String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
