package ru.jborn.core.golub.alexander.lesson6;


import java.io.*;

public class FileService {

    public void isValidOutputFile( File file ) throws CantWriteInFile {
        if (!(file.exists() && file.canWrite())) {
            throw new CantWriteInFile("Что-то не так с файлом для записи", null, false, false);
        }
    }

    public String readFile( String pathToFile ) throws IOException, CantReadException {
        String readed = " ";
        File file = new File(pathToFile);

        if (file.exists()) {
            if (file.canRead()) {
                try (BufferedReader br = new BufferedReader(new FileReader(pathToFile))) {
                    if ((readed = br.readLine()) != null) {
                        if (InputLineValidator.validation(readed))
                            return readed;
                    }
                } catch (EOFException ex) {
                    return ex + " нет строк для чтения";
                }
            } else {
                throw new CantReadException("Нет прав на чтение файла");
            }
        } else {
            throw new FileNotFoundException();
        }
        return readed;
    }

    public void writeInFile( String val, String pathToFile ) throws CantWriteInFile {
        try (FileWriter fw = new FileWriter(pathToFile)) {
            fw.write(val);
            fw.flush();
        } catch (IOException e) {
            throw new CantWriteInFile("Не получилось записать в файл ", e, false, false);
        }
    }
}




