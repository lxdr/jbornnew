package ru.jborn.core.golub.alexander.lesson6;

import javax.swing.*;

public class InputLineValidator {

    public static boolean validation( String line ) throws NumberFormatException {
        boolean result = false;
        try {
            Integer.parseInt(line);
            result = true;
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(null, "В строке есть символы кроме цифр");
        }
        return result;
    }
}
