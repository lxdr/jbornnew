package ru.jborn.core.golub.alexander.lesson6;


import javax.swing.*;
import java.io.File;
import java.io.IOException;

public class Task {

    public static void main( String[] args ) throws IOException {
        FileService fileService = new FileService();
        String val = null;
        try {
            fileService.isValidOutputFile(new File(args[1]));
            try {
                val = fileService.readFile(args[0]);
                val = WorkerWithFileDada.sumInString(val);
                fileService.writeInFile(val, args[1]);
            } catch (CantReadException e) {
                JOptionPane.showMessageDialog(null, "Нет прав на чтение файла");
            } catch (CantWriteInFile cantWriteInFile) {
                JOptionPane.showMessageDialog(null, "Не получилось записать в файл");
            }
        } catch (CantWriteInFile cantWriteInFile) {
            JOptionPane.showMessageDialog(null, "Не получилось записать в файл" + args[1]);
        } finally {
            System.out.print(val);
        }
    }
}
