package ru.jborn.core.golub.alexander.lesson6;

public class WorkerWithFileDada {

    private static int iternalSum( int val ) {
        int sum = 0;
        while (val != 0) {
            sum = sum + (val % 10);
            val = val / 10;
        }
        return sum;
    }

    public static int sum( String arg ) {
        arg = arg.replaceAll(" ", "");
        int val = Integer.parseInt(arg);
        return iternalSum(val);
    }

    public static int sum( int arg ) {
        return iternalSum(arg);
    }

    public static String sumInString( String arg ) {
        int val = sum(arg);
        return String.valueOf(val);
    }
}
