package ru.jborn.core.golub.alexander.lesson7.task1;

import javax.swing.*;

public class SingleLinkedList<T> {
    private Entity<T> head;
    private Entity<T> current;
    private int size = 0;

    SingleLinkedList( T val ) {
        head = new Entity<>(val);
        current = head;
        size++;
    }

    public void add( T val ) {
        Entity<T> next = new Entity<>(val);
        current.next = next;
        current = next;
        size++;
    }

    public void isCycledON( int element ) {
        Entity<T> current = head;
        JOptionPane.showMessageDialog(null, "Зацикленность включена на " + element + " элементе");
        int k = 1;
        while (k < length()) {
            current = current.next;
            if (k == element) {
                current.next = head;
            }
            k++;
        }
    }

    public int length() {
        return size;
    }

    public T get( int i ) {
        Entity<T> current = head;
        int k = 0;
        while (k < i) {
            current = current.next;
            k++;
        }
        return current.value;
    }

    public Entity<T> getLink( int i ) {
        Entity<T> current = head;
        int k = 0;
        while (k < i) {
            current = current.next;
            k++;
        }
        return current.next;
    }

    public boolean isCycled() {
        Entity<T> first = head;
        Entity<T> second = head.next.next;
        while (second != null) {
            if (first == second) {
                return true;
            }
            first = first.next;
            second = second.next.next;
        }
        return false;
    }

    private static class Entity<T> {
        private T value;
        private Entity<T> next;

        Entity( T value ) {
            this.value = value;
        }
    }
}

