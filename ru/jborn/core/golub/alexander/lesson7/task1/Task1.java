package ru.jborn.core.golub.alexander.lesson7.task1;

public class Task1 {

    public static void main( String[] args ) {

        SingleLinkedList<Integer> loader = new SingleLinkedList<>(0);
        for (int i = 1; i < 100; i++) {
            loader.add(i);
        }
        System.out.println(loader.get(20));
        System.out.println(loader.isCycled());

        loader.isCycledON(10);

        System.out.println(loader.get(20));
        System.out.print(loader.isCycled());
    }

}
