package ru.jborn.core.golub.alexander.lesson7.task2;

import java.util.LinkedList;
import java.util.List;

public class ArrayService<T> {
    private T[] arr;

    public ArrayService(T[] arr) {
        this.arr = arr;
    }

    List<String> convertToListString(long size) {
        List<String> list = new LinkedList<>();
        int k = 0;
        for (int i = 0; k < size; i++) {
            if (i == 252) {
                i = 0;
            }
            if (k < 252) {
                list.add((String) arr[i]);
                k++;
            } else {
                list.add(k + "\t\t\t" + arr[i]);
                k++;
            }
        }
        return list;
    }
}
