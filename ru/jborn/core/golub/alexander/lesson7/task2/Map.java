package ru.jborn.core.golub.alexander.lesson7.task2;


public interface Map<K, V> {
    int size();

    boolean containsKey( K key );

    boolean containsValue( V value );

    V get( K key );

    V put( K key, V value );

    V remove( K key );
}