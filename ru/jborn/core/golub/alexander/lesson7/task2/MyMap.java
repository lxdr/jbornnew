package ru.jborn.core.golub.alexander.lesson7.task2;


import java.util.ArrayList;
import java.util.List;

public class MyMap<K, V> implements Map<K, V> {
    private static final int COEFFICIENT_OF_RESIZE = 3 / 2 + 1;
    private static final double TABLE_CAPACITY = 0.7;
    private int size = 0;
    private List<Node<K, V>>[] table;

    public MyMap() {
        // Стандартный размер массива - 10
        table = new ArrayList[15];
    }

    private int indexByHash(K key) {
        return Math.abs(key.hashCode() % table.length);
    }

    private void resize() {
        int newSize = size * COEFFICIENT_OF_RESIZE;
        int index;
        List<Node<K, V>>[] tableDonor = table;
        table = new ArrayList[newSize];
        for (List<Node<K, V>> listNodes : tableDonor) {
            if (listNodes != null) {
                for (Node<K, V> node : listNodes) {
                    index = indexByHash(node.key);
                    put(node.key, node.value, index);
                }
            }
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean containsKey(K key) {
        return get(key) != null;
    }

    @Override
    public boolean containsValue(V value) {
        for (List<Node<K, V>> list : table) {
            if (list != null) {
                for (Node<K, V> node : list) {
                    if (value.equals(node.value)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public V get(K key) {
        int index = indexByHash(key);
        return get(key, index);
    }

    private V get(K key, int index) {
        Node<K, V> node = getNode(key, index);
        if (node != null) {
            return node.value;
        }
        return null;
    }

    private Node<K, V> getNode(K key, int index) {
        if (table[index] != null) {
            for (int i = 0; i < table[index].size(); i++) {
                if (table[index].get(i).key != null && table[index].get(i).key.equals(key)) {
                    return table[index].get(i);
                }
            }
        }
        return null;
    }

    @Override
    public V remove(K key) {
        Node<K, V> node = getNode(key, indexByHash(key));
        V lastValue = null;
        if (node != null) {
            lastValue = node.value;
            node.value = null;
            node.key = null;
            size--;
        }
        return lastValue;
    }

    @Override
    public V put(K key, V value) {
        V lastValue;
        int index = indexByHash(key);

        if ((lastValue = get(key, index)) != null && getNode(key, index).key.equals(key)) {
            getNode(key, index).value = value;
        } else {
            put(key, value, index);
            size++;
            if (size > table.length * TABLE_CAPACITY) {
                resize();
            }
        }
        return lastValue;
    }

    private void put(K key, V value, int index) {
        if (table[index] == null) {
            table[index] = new ArrayList<>();
        }
        table[index].add(new Node<>(key, value));
    }

    private class Node<K, V> {
        K key;
        V value;

        Node(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
}