package ru.jborn.core.golub.alexander.lesson7.task2;


import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;


public class TestForMyMap extends MultiThreadTestMap implements Runnable {

    private String name = " MyMap ";
    private long testSize;
    private LinkedList<String> listOfSur;
    private MyMap<String, String> numberSurnameMap;

    TestForMyMap(long testSize) {
        this.testSize = testSize;
        ArrayService<String> arrayService = new ArrayService<>(Data.list);
        listOfSur = (LinkedList<String>) arrayService.convertToListString(testSize);
        numberSurnameMap = new MyMap<>();
        Thread t = new Thread(this, name);
        t.start();
    }

    @Override
    public void run() {
        synchronized (timer) {
            try (FileWriter writer = new FileWriter(timer, true)) {
                Iterator<String> iteratorKey = listOfSur.iterator();
                Iterator<String> iteratorVal = listOfSur.iterator();

                long timeStart = System.currentTimeMillis();
                for (int i = 0; i < testSize; i++) {
                    numberSurnameMap.put(iteratorKey.next(), iteratorVal.next());
                }
                long timeEnd = System.currentTimeMillis();
                long leadTime = timeEnd - timeStart;
                writer.append("\n\n")
                        .append(name)
                        .append(" время put ")
                        .append(String.valueOf(leadTime))
                        .append(" msec | количество элементов ")
                        .append(String.valueOf(testSize));

                iteratorKey = listOfSur.iterator();
                timeStart = System.currentTimeMillis();
                for (int i = 0; i < testSize%10; i++) {
                    numberSurnameMap.remove(iteratorKey.next());
                }
                timeEnd = System.currentTimeMillis();
                leadTime = timeEnd - timeStart;
                writer.append("\n")
                        .append(name)
                        .append(" время remove ")
                        .append(String.valueOf(leadTime))
                        .append(" msec");
                System.out.println("deleted " + testSize%10);

                iteratorKey = listOfSur.iterator();
                timeStart = System.currentTimeMillis();
                for (int i = 0; i < numberSurnameMap.size(); i++) {
                   numberSurnameMap.get(iteratorKey.next());
                }
                timeEnd = System.currentTimeMillis();
                System.out.print("\n" + numberSurnameMap.size()
                        + " элементов в " + name + " \n");
                leadTime = timeEnd - timeStart;
                writer.append("\n")
                        .append(name)
                        .append(" время get ")
                        .append(String.valueOf(leadTime))
                        .append(" msec");

            } catch (IOException e) {
                System.out.println("Не удалось записать в файл");
            }
        }
    }
}