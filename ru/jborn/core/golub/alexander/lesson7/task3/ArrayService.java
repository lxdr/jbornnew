package ru.jborn.core.golub.alexander.lesson7.task3;

import ru.jborn.core.golub.alexander.lesson7.task3.binarySearch.BinarySearch;
import ru.jborn.core.golub.alexander.lesson7.task3.sort.*;

import java.util.Random;


public class ArrayService {

    private int[] randomIntArray;

    public ArrayService(int size, boolean random) {
        if (random) {
            randomIntArray = produceRandomIntArray(size);
        }
    }

    private int[] produceRandomIntArray(int size) {
        Random random = new Random();
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            array[i] = random.nextInt(size);
        }
        return array;
    }

    public void show(boolean horizontal) {
        if (horizontal) {
            for (int elem :
                    randomIntArray) {
                System.out.print(elem + " ");
            }
            System.out.println();
        } else {
            for (int elem :
                    randomIntArray) {
                System.out.println(elem);
            }
        }
    }

    public boolean isSort() {
        for (int i = 1; i < randomIntArray.length; i++) {
            if (randomIntArray[i] < randomIntArray[i - 1]) {
                return false;
            }
        }
        return true;
    }

    public void quickSort(boolean ascending) {

        long timeStart = System.currentTimeMillis();
        if (ascending) {
            Sort quick = new Quick();
            quick.sort(randomIntArray);
        } else {
            Sort quickDescending = new QuickDescending();
            quickDescending.sort(randomIntArray);
        }
        long timeEnd = System.currentTimeMillis();
        System.out.println("type QUICK " + " time " + (timeEnd - timeStart));
    }

    public void insertSort() {
        Sort insert = new Insert();
        long timeStart = System.currentTimeMillis();
        insert.sort(randomIntArray);
        long timeEnd = System.currentTimeMillis();
        System.out.println("type INSERT " + " time " + (timeEnd - timeStart));
    }

    public void selectionSort() {
        Sort selection = new Selection();
        long timeStart = System.currentTimeMillis();
        selection.sort(randomIntArray);
        long timeEnd = System.currentTimeMillis();
        System.out.println("type SELECTION " + " time " + (timeEnd - timeStart));
    }

    public void mergeSort() {
        Sort merge = new Merge();
        long timeStart = System.currentTimeMillis();
        merge.sort(randomIntArray);
        long timeEnd = System.currentTimeMillis();
        System.out.println("type MERGE " + " time " + (timeEnd - timeStart));
    }

    public int search(int key) {
        return BinarySearch.binarySearch(randomIntArray, key);
    }
}
