package ru.jborn.core.golub.alexander.lesson7.task3;

public class CompareSort {
    public void compare(String method1, String method2, int size) {
        switch (method1) {
            case "insert":
                ArrayService forInsertSort = new ArrayService(size, true);
                forInsertSort.insertSort();
                System.out.println(forInsertSort.isSort());
                break;
            case "selection":
                ArrayService forSelectionSort = new ArrayService(size, true);
                forSelectionSort.selectionSort();
                System.out.println(forSelectionSort.isSort());
                break;
            case "quick":
                ArrayService forQuickSort = new ArrayService(size, true);
                forQuickSort.quickSort(true);
                System.out.println(forQuickSort.isSort());
                break;
            case "merge":
                ArrayService forMergeSort = new ArrayService(size, true);
                forMergeSort.mergeSort();
                System.out.println(forMergeSort.isSort());
                break;
        }

        switch (method2) {
            case "insert":
                ArrayService forInsertSort = new ArrayService(size, true);
                forInsertSort.insertSort();
                System.out.println(forInsertSort.isSort());
                break;
            case "selection":
                ArrayService forSelectionSort = new ArrayService(size, true);
                forSelectionSort.selectionSort();
                System.out.println(forSelectionSort.isSort());
                break;
            case "quick":
                ArrayService forQuickSort = new ArrayService(size, true);
                forQuickSort.quickSort(true);
                System.out.println(forQuickSort.isSort());
                break;
            case "merge":
                ArrayService forMergeSort = new ArrayService(size, true);
                forMergeSort.mergeSort();
                System.out.println(forMergeSort.isSort());
                break;
        }
    }
}
