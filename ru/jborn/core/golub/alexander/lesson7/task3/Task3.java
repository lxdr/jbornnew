package ru.jborn.core.golub.alexander.lesson7.task3;

public class Task3 {
    public static void main(String[] args) {
        CompareSort compare1 = new CompareSort();
        CompareSort compare2 = new CompareSort();

        compare1.compare("insert", "selection", 10000);
        compare2.compare("quick", "merge", 10000);

        ArrayService arrayForBinarySearch = new ArrayService(10, true);
        arrayForBinarySearch.show(true);
        arrayForBinarySearch.quickSort(false);
        arrayForBinarySearch.show(true);
        System.out.println(arrayForBinarySearch.search(77));
    }
}