package ru.jborn.core.golub.alexander.lesson7.task3.binarySearch;

public class BinarySearch {

    private static int binarySearchIncr(int[] array, int key, int low, int high) {
        int mid = low + (high - low) / 2;
        if (low >= high) {
            return -(1 + low);
        }
        if (array[mid] == key) {
            return mid;
        }
        if (array.length == 1) {
            return -1;
        }
        if (key > array[mid]) {
            return binarySearchIncr(array, key, mid + 1, high);
        } else {
            return binarySearchIncr(array, key, low, mid);
        }
    }


    private static int binarySearchDecr(int[] array, int key, int low, int high) {
        int mid = low + (high - low) / 2;
        if (low >= high) {
            return -(1 + low);
        }
        if (array[mid] == key) {
            return mid;
        }
        if (array.length == 1) {
            return -1;
        }
        if (key < array[mid]) {
            return binarySearchDecr(array, key, mid + 1, high);
        } else {
            return binarySearchDecr(array, key, low, mid);
        }
    }

    public static int binarySearch(int[] array, int key) {
        if (array[0] < array[array.length - 1]) {
            return binarySearchIncr(array, key, 0, array.length);
        } else {
            return binarySearchDecr(array, key, 0, array.length - 1);
        }
    }
}