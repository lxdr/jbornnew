package ru.jborn.core.golub.alexander.lesson7.task3.sort;

public class Insert extends Sort {

    @Override
    public void sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i; j > 0 && (array[j] < array[j - 1]); j--) {
                Sort.xchng(array, j, j - 1);
            }
        }
    }
}