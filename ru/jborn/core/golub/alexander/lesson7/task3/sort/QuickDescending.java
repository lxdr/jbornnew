
package ru.jborn.core.golub.alexander.lesson7.task3.sort;

public class QuickDescending extends Sort {

    @Override
    public void sort(int[] array) {
        sort(array, 0, array.length - 1);
    }

    private void sort(int[] array, int low, int high) {
        if (array.length == 0) {
            return;
        }
        if (low >= high) {
            return;
        }
        int middle = low + (high - low) / 2;
        int i = low, j = high;
        while (i < j) {
            while (i < middle && array[i] >= array[middle]) {
                i++;
            }
            while (j > middle && array[j] <= array[middle]) {
                j--;
            }
            //меняем местами
            if (i < j) {
                Sort.xchng(array, i, j);
                if (i == middle) {
                    middle = j;
                } else {
                    if (j == middle) {
                        middle = i;
                    }
                }
            }
        }
        // вызов рекурсии для сортировки левой и правой части
        sort(array, low, middle);
        sort(array, middle + 1, high);
    }
    //самостоятельно написать не получиться
}