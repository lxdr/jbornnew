package ru.jborn.core.golub.alexander.lesson7.task3.sort;

public class Selection extends Sort {

    @Override
    public void sort(int[] array) {
        sort(array, 0, array.length);
    }

    private void sort(int[] array, int low, int high) {
        //правильно ли я думаю, что создав переменную тут, а не в цикле будет задействовано меньше памяти на: количество итераций * 4 byte
        int k;
        for (int i = 0; i < array.length; i++) {
            k = i;
            for (int j = i; j < array.length; j++) {
                if (array[j] < array[k]) {
                    k = j;
                }
            }
            Sort.xchng(array, i, k);
        }
    }
}