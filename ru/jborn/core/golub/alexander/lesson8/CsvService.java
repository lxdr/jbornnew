package ru.jborn.core.golub.alexander.lesson8;


import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

//видимо не до конца разобрался с дженериками. Не получается сделать метод csvToList дженерифицированным
public class CsvService {

    private void formatValidator(File file) {
        if (!Pattern.matches("(?i)csv", file.getName().substring(file.getName().length() - 3))) {
            throw new IllegalArgumentException("Неверный формат файла");
        }
    }

    //не могу понять, почему в данном случае List<Employee> не равнозначно List<? extends employee>
    public List<Employee> csvToListEmployee(File file) throws IllegalArgumentException {
        int count = 0;
        List<Employee> list = new ArrayList<>();
        formatValidator(file);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file),
                "CP1251"))) {
            {
                String line;
                while ((line = br.readLine()) != null) {
                    if (count == 0) {
                        count++;
                        continue;
                    }
                    list.add(doConvert(line));
                    count++;
                }
            }
        } catch (FileNotFoundException f) {
            System.err.println("File not found");
        } catch (IOException e) {
            System.err.println(e);
        }
        return list;
    }

    private Employee doConvert(String line) {
        Pattern pattern = Pattern.compile(";");
        String[] employerInArrayView = pattern.split(line);
        Employee empl = new Employee();
        empl.setFirstName(employerInArrayView[0]);
        empl.setMiddleName(employerInArrayView[1]);
        empl.setLastName(employerInArrayView[2]);
        DateTimeFormatter df = DateTimeFormatter.ofPattern("d.MM.yyyy");
        empl.setBirthDay(LocalDate.parse(employerInArrayView[3], df));
        Position pos;
        switch (employerInArrayView[4].toUpperCase()) {
            case "ДИРЕКТОР":
                pos = Position.DIRECTOR;
                break;
            case "МЕНЕДЖЕР":
                pos = Position.MANAGER;
                break;
            case "ПРОГРАММИСТ":
                pos = Position.PROGRAMMER;
                break;
            case "АНАЛИТИК":
                pos = Position.ANALYST;
                break;
            case "СЕКРЕТАРЬ":
                pos = Position.SCRETARY;
                break;
            default:
                throw new IllegalArgumentException("Неверный аргумент " + employerInArrayView[4]);
        }
        empl.setPosition(pos);
        empl.setWages(Integer.parseInt(employerInArrayView[5]));
        return empl;
    }
}