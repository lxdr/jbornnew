package ru.jborn.core.golub.alexander.lesson8;

import ru.jborn.core.golub.alexander.lesson8.exceptions.FileWriteExceprion;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DbService {

    public DbService(File db) throws IOException {
        if (!dbFileValidation(db)) {
            if (new File(db.getAbsolutePath().substring(0, db.getAbsolutePath().lastIndexOf('/'))).canWrite()) {
                db.createNewFile();
            }
        }
    }

    private boolean dbFileValidation(File db) {
        if (db.exists() && db.canWrite()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean compareByName(Employee e1, Employee e2) {
        if (!e1.getFirstName().equals(e2.getFirstName())) return false;
        if (!e1.getMiddleName().equals(e2.getMiddleName())) return false;
        if (!e1.getLastName().equals(e2.getMiddleName())) return false;
        return e1.getBirthDay().equals(e2.getBirthDay());
    }


    private List<Employee> doMerge(List<Employee> bigList, List<Employee> smallList) {
        for (int i = 0; i < bigList.size(); i++) {
            for (int j = 0; j < smallList.size(); j++) {
                if (compareByName(bigList.get(i), smallList.get(j))) {
                    bigList.set(i, smallList.get(j));
                    break;
                }
            }
        }
        return bigList;
    }

    private void writeInFile(File db, List externalList) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(db))) {
            oos.writeObject(externalList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<Employee> mergedList(List<Employee> listFromDb, List<Employee> externalList) {
        if (listFromDb.size() > externalList.size()) {
            return doMerge(listFromDb, externalList);
        } else {
            return doMerge(externalList, listFromDb);
        }
    }

    public void push(File db, List externalList) throws IOException, FileWriteExceprion {
        if (dbFileValidation(db)) {
            List listEmployeesFromDb = getListEmployeesFromDb(db);
            if (listEmployeesFromDb != null) {
                writeInFile(db, mergedList(listEmployeesFromDb, externalList));
            } else {
                writeInFile(db, externalList);
            }
        } else {
            throw new FileWriteExceprion("База данных перестала существовать или в неё невозможно записать данные");
        }
    }

    private ArrayList<Employee> getListEmployeesFromDb(File db) throws IOException {
        ArrayList<Employee> list = null;
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(db))) {
            list = (ArrayList<Employee>) ois.readObject();
        } catch (Exception e) {
            System.err.println("ois " + e.getMessage());
        }
        return list;
    }

    public void show(File db) {
        List<Employee> list;
        try {
            if ((list = getListEmployeesFromDb(db)) != null) {
                list.forEach(e -> {
                    if (e != null) {
                        System.out.println(e.toString());
                    }
                });
            }
        } catch (IOException e) {
            System.err.println("Не получилось взять список " + e);
        }
    }
}