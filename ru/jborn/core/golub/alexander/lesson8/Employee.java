package ru.jborn.core.golub.alexander.lesson8;


import java.io.Serializable;
import java.time.LocalDate;

public class Employee implements Serializable {
    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    private String firstName;
    private String middleName;
    private String lastName;
    private LocalDate birthDay;
    private Position position;
    private int wages;

    public Employee() {
    }

    public Employee(String firstName, String middleName,
                    String lastName, LocalDate birthDay,
                    Position position, int wages) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.birthDay = birthDay;
        this.position = position;
        this.wages = wages;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }


    public void setPosition(Position position) {
        this.position = position;
    }


    public void setWages(int wages) {
        this.wages = wages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (wages != employee.wages) return false;
        if (firstName != null ? !firstName.equals(employee.firstName) : employee.firstName != null) return false;
        if (middleName != null ? !middleName.equals(employee.middleName) : employee.middleName != null) return false;
        if (lastName != null ? !lastName.equals(employee.lastName) : employee.lastName != null) return false;
        if (birthDay != null ? !birthDay.equals(employee.birthDay) : employee.birthDay != null) return false;
        return position != null ? position.equals(employee.position) : employee.position == null;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (birthDay != null ? birthDay.hashCode() : 0);
        result = 31 * result + (position != null ? position.hashCode() : 0);
        result = 31 * result + wages;
        return result;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' + "," + "\t"+
                "middleName='" + middleName + '\'' + "," +  "\t"+
                "lastName='" + lastName + '\'' + "," + "\t"+
                "birthDay=" + birthDay + "," + "\t"+
                "position=" + position + "," + "\t"+
                "wages=" + wages +
                '}';
    }

}
