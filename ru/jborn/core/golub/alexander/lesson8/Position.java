package ru.jborn.core.golub.alexander.lesson8;

public enum Position {
    DIRECTOR("директор"),
    MANAGER("менеджер"),
    PROGRAMMER("программист"),
    ANALYST("аналитик"),
    SCRETARY("секретарь");

    private String type;

    public String getType() {
        return type;
    }

    Position(String type) {
        this.type = type;
    }
}
