package ru.jborn.core.golub.alexander.lesson8;

import ru.jborn.core.golub.alexander.lesson8.exceptions.FileWriteExceprion;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Task1 {
    public static void main(String[] args) {
        File fileCsv = new File("/home/tiric/ideaProjects/src/ru/jborn/core/golub/alexander/lesson8/Employee.csv");
        File fileDb = new File("/home/tiric/ideaProjects/src/ru/jborn/core/golub/alexander/lesson8/Employee.dat");
        CsvService csvService = new CsvService();
        DbService dbService = null;
        try {
            dbService = new DbService(fileDb);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Employee> employeesFromCsv = csvService.csvToListEmployee(fileCsv);
        try {
            dbService.push(fileDb, employeesFromCsv);
        } catch (IOException e) {
            System.out.println("Ошибка ввода вывода");
        } catch (FileWriteExceprion fileWriteExceprion) {
            System.out.println(fileWriteExceprion.toString());
        }
        System.out.println();
        dbService.show(fileDb);
    }
}