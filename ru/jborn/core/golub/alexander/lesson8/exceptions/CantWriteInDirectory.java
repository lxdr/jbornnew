package ru.jborn.core.golub.alexander.lesson8.exceptions;

public class CantWriteInDirectory extends RuntimeException {
    public CantWriteInDirectory(String s){
        super("Нет прав для записи в директорию " + s);
    }
}
