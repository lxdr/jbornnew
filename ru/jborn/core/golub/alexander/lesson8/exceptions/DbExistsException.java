package ru.jborn.core.golub.alexander.lesson8.exceptions;

public class DbExistsException extends RuntimeException {
    public DbExistsException(String s){
        super(s);
    }

}
